#include "stdafx.h"
#include "CppUnitTest.h"
#include "../Baseball/Team.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Baseball;

namespace TeamTest
{		
	TEST_CLASS(TeamTest){
	public:
		TEST_METHOD(Add){
			Team team;
			for (int i = 0; i != 100; i++){
				Assert::AreEqual(team.add_id(i), true);
			}
			auto ft = team.get_order(0);
			auto st = team.get_order(1);
			auto s = team.get_order(2);
			
			Assert::AreEqual(static_cast<int>(ft.size()), 18);
			Assert::AreEqual(static_cast<int>(st.size()), 18);
			Assert::AreEqual(static_cast<int>(s.size()), 64);
		}
		TEST_METHOD(Add_Find_Del_Find){
			// TODO: テスト コードをここに挿入します
			Team team;
			Assert::AreEqual(team.add_id(1), true);
			auto t = team.get_order(0);
			auto& m = t.get<Team::id>();
			Assert::AreEqual((m.find(1) != m.end()), true);
			
			Assert::AreEqual(team.del_id(1), true);
			t = team.get_order(0);
			m = t.get<Team::id>();
			Assert::AreEqual((m.find(1) != m.end()), false);
		}
		TEST_METHOD(change_dajun){
			Team team;
			for (int i = 0; i != 100; i++){
				team.add_id(i);
			}
			auto ft = team.get_order(0);
			auto& fm = ft.get<Team::dajun>();
			auto st_3 = (*fm.find(3));
			auto st_7 = (*fm.find(7));

			Assert::AreEqual(team.change_dajun(3, 7, 0), true);

			ft = team.get_order(0);
			fm = ft.get<Team::dajun>();

			Assert::AreEqual((*fm.find(7)).id, st_3.id);
			Assert::AreEqual((*fm.find(3)).id, st_7.id);

		}
		TEST_METHOD(change_pos){
			Team team;
			for (int i = 0; i != 100; i++){
				team.add_id(i);
			}
			auto ft = team.get_order(0);
			auto& fm = ft.get<Team::pos>();
			auto st_3 = (*fm.find(3));
			auto st_7 = (*fm.find(7));

			Assert::AreEqual(team.change_pos(3, 7, 0), true);

			ft = team.get_order(0);
			fm = ft.get<Team::pos>();

			Assert::AreEqual((*fm.find(7)).id, st_3.id);
			Assert::AreEqual((*fm.find(3)).id, st_7.id);
		}
		TEST_METHOD(change_id){
			Team team;
			for (int i = 0; i != 100; i++){
				team.add_id(i);
			}
			auto ft = team.get_order(0);
			auto& fm = ft.get<Team::id>();
			auto st = team.get_order(2);
			auto& sm = st.get<Team::id>();
			auto s_6 = (*fm.find(6));
			auto s_60 = (*sm.find(60));

			Assert::AreEqual(team.change_id(6, 0, 60, 2), true);

			ft = team.get_order(0);
			fm = ft.get<Team::id>();
			st = team.get_order(2);
			sm = st.get<Team::id>();

			Assert::AreEqual((*fm.find(60)).pos, s_6.pos);
			Assert::AreEqual((*sm.find(6)).pos, s_60.pos);
		}
	};
}