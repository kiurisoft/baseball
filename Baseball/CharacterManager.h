#pragma once

#include <iostream>
#include <unordered_map>
#include <memory>
#include <fstream>
#include <boost\serialization\serialization.hpp>
#include <boost\serialization\split_member.hpp>
#include <boost\archive\binary_iarchive.hpp>
#include <boost\archive\binary_oarchive.hpp>
#include <random_wrapping.h>

#include "Character.h"

using namespace std;

namespace Baseball{
	class CharacterManager{
	private:
		unordered_map<int, shared_ptr<Character>> chars;
		
		friend class boost::serialization::access;
		BOOST_SERIALIZATION_SPLIT_MEMBER();
		
		template<class Archive>
		void save(Archive& arc) const {
			arc & chars.size();
			for (auto& temp : chars){
				arc & temp.first;
				arc & temp.second.get();
			}
		}

		template<class Archive>
		void load(Archive& arc){
			int size = 0;
			arc & size;
			for (int i = 0; i != size; i++){
				int id = 0;
				Character* temp = nullptr;
				arc & id;
				arc & temp;
				chars[id].reset(temp);
			}
		}
	public:
		shared_ptr<Character> get(int id);
		void destroy(int id);
		shared_ptr<Character> create();
		void load_chars(string path);
		void save_chars(string path);
	};
}