#pragma once

namespace Baseball{
	class Count{
	private:
		int ball;
		int strike;
		int out;
	public:
		Count() :ball(0), strike(0), out(0){}

		bool add_ball();
		bool add_strike();
		bool add_out();
		void reset_all();
		void reset_bs();

		int get_ball();
		int get_strike();
		int get_out();
	};
}