#include "Character.h"

Baseball::Character::Character(int id_) :id(id_){

}

void Baseball::Character::init(){

}

Baseball::Character::c_param_ Baseball::Character::pro_c_param(Baseball::Character::c_param_& par){
	c_param.daijuidou += par.daijuidou;
	c_param.jouhanshin += par.jouhanshin;
	c_param.kahanshin += par.kahanshin;
	c_param.kuukan += par.kuukan;
	c_param.mental += par.mental;
	c_param.shuutyuu += par.shuutyuu;
	c_param.stamina += par.stamina;
	return c_param;
}

Baseball::Character::f_param_ Baseball::Character::pro_f_param(Baseball::Character::f_param_& par){
	f_param.batcontrol += par.batcontrol;
	f_param.dandou += par.dandou;
	for (int i = 0; i != 9; i++){
		f_param.def[i] += par.def[i];
	}
	f_param.doutai += par.doutai;
	f_param.kasoku += par.kasoku;
	f_param.sokudo += par.sokudo;
	f_param.tekisei += par.tekisei;
	return f_param;
}

Baseball::Character::p_param_ Baseball::Character::pro_p_param(Baseball::Character::p_param_& par){
	for (int i = 0; i != 2; i++){
		for (int j = 0; j != 6; j++){
			p_param.bb[j][i].tate += par.bb[j][i].tate;
			p_param.bb[j][i].yoko += par.bb[j][i].yoko;
			p_param.bb[j][i].control += par.bb[j][i].control;
			p_param.bb[j][i].teikou += par.bb[j][i].teikou;
			p_param.bb[j][i].difficulty += par.bb[j][i].difficulty;
		}
	}
	p_param.kiyou += par.kiyou;
	p_param.tekisei += par.tekisei;
	return p_param;
}

Baseball::Character::c_stats_ Baseball::Character::pro_c_stats(Baseball::Character::c_stats_& sta){
	c_stats.age += sta.age;
	c_stats.daseki += sta.daseki;
	c_stats.kikite += sta.kikite;
	c_stats.term += sta.term;
	return c_stats;
}

Baseball::Character::f_stats_ Baseball::Character::pro_f_stats(Baseball::Character::f_stats_& sta){
	for (int i = 0; i != 2; i++){
		f_stats.dasuu[i] += sta.dasuu[i];
		f_stats.sansin[i] += sta.sansin[i];
		f_stats.fourball[i] += sta.fourball[i];
		f_stats.deadball[i] += sta.deadball[i];
		f_stats.gida[i] += sta.gida[i];
		f_stats.gihi[i] += sta.gihi[i];
		f_stats.tanda[i] += sta.tanda[i];
		f_stats.niruida[i] += sta.niruida[i];
		f_stats.sanruida[i] += sta.sanruida[i];
		f_stats.honruida[i] += sta.honruida[i];
		f_stats.heisatu[i] += sta.heisatu[i];
		f_stats.daten[i] += sta.daten[i];
	}
	f_stats.tokuten += sta.tokuten;
	f_stats.touruikito += sta.touruikito;
	f_stats.touruiseikou += sta.touruiseikou;
	for (int i = 0; i != 9; i++){
		f_stats.hosatu[i] += sta.hosatu[i];
		f_stats.sisatu[i] += sta.sisatu[i];
		f_stats.shubikikai[i] += sta.shubikikai[i];
		f_stats.error[i] += sta.error[i];
	}
	return f_stats;
}

Baseball::Character::p_stats_ Baseball::Character::pro_p_stats(Baseball::Character::p_stats_& sta){
	for (int i = 0; i != 2; i++){
		p_stats.touban[i] += sta.touban[i];
		p_stats.datusanshin[i] += sta.datusanshin[i];
		p_stats.fourball[i] += sta.fourball[i];
		p_stats.deadball[i] += sta.deadball[i];
		p_stats.hitanda[i] += sta.hitanda[i];
		p_stats.hiniruida[i] += sta.hiniruida[i];
		p_stats.hisanruida[i] += sta.hisanruida[i];
		p_stats.hihonruida[i] += sta.hihonruida[i];
		p_stats.hurai[i] += sta.hurai[i];
		p_stats.goro[i] += sta.goro[i];
		p_stats.heisatu[i] += sta.heisatu[i];
		p_stats.sitten[i] += sta.sitten[i];
		p_stats.jiseki[i] += sta.jiseki[i];
		for (int j = 0; j != 2; j++){
			p_stats.inning[i][j] += sta.inning[i][j];
		}
	}
	p_stats.win += sta.win;
	p_stats.lose += sta.lose;
	p_stats.hold += sta.hold;
	p_stats.save += sta.save;
	return p_stats;
}