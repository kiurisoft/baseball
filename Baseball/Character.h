#pragma once

#include <iostream>
#include <boost\serialization\serialization.hpp>
#include <boost\serialization\string.hpp>

namespace Baseball{
	class Character{
	public:
		struct c_param_{
			int stamina;
			int jouhanshin;
			int kahanshin;
			int mental;
			int kuukan;
			int daijuidou;
			int shuutyuu;
		};
		struct f_param_{
			int tekisei;
			int doutai;
			int batcontrol;
			int dandou;
			int kasoku;
			int sokudo;
			int def[9];
		};
		struct bb_param_{
			int tate;
			int yoko;
			int control;
			int teikou;
			int difficulty;
		};
		struct p_param_{
			int tekisei;
			int kiyou;
			bb_param_ bb[6][2];
		};
		struct c_stats_{
			int age;
			int term;
			int kikite;
			int daseki;//right or left or both
		};
		struct f_stats_{
			int dasuu[2];
			int sansin[2];
			int fourball[2];
			int deadball[2];
			int gida[2];
			int gihi[2];
			int tanda[2];
			int niruida[2];
			int sanruida[2];
			int honruida[2];
			int heisatu[2];
			int daten[2];
			int tokuten;
			int touruikito;
			int touruiseikou;
			int hosatu[9];
			int sisatu[9];
			int shubikikai[9];
			int error[9];
		};
		struct p_stats_{
			int touban[2];
			int inning[2][2];
			int datusanshin[2];
			int fourball[2];
			int deadball[2];
			int hitanda[2];
			int hiniruida[2];
			int hisanruida[2];
			int hihonruida[2];
			int hurai[2];
			int goro[2];
			int heisatu[2];
			int sitten[2];
			int jiseki[2];
			int win;
			int lose;
			int hold;
			int save;
		};
	private:
		c_param_ c_param;
		f_param_ f_param;
		p_param_ p_param;

		c_stats_ c_stats;
		f_stats_ f_stats;
		p_stats_ p_stats;

		std::string first_name, family_name;
		int id;

		friend class boost::serialization::access;
		template<class Archive>
		void serialize(Archive& arc){
			arc & c_param & f_param & p_param;
			arc & c_stats & f_stats & p_stats;
			arc & first_name & family_name;
			arc & id;
		}
	public:
		Character(int id_);
		Character(){};

		void init();
		c_param_ pro_c_param(c_param_& par);
		f_param_ pro_f_param(f_param_& par);
		p_param_ pro_p_param(p_param_& par);

		c_stats_ pro_c_stats(c_stats_& sta);
		f_stats_ pro_f_stats(f_stats_& sta);
		p_stats_ pro_p_stats(p_stats_& sta);
	};
}