#pragma once

#include <string>
#include <random>

#include "CharacterManager.h"
#include "Team.h"
#include "Count.h"

namespace Baseball{
	class Match{
	private:
		Team::order_ order[2];
		Count count;
		bool finished;

		bool tourui(std::string& player_select);
		bool kensei(std::string& player_select);
		void toukyu(std::string& player_select);
		void dakyu(std::string& player_select);
		void shubi(std::string& player_select);
	public:
		Match(Team::order_& f, Team::order_& l);

		bool is_finished();
		void step(std::string& player_select);
		//log
	};
}