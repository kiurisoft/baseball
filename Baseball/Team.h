#pragma once

#include "CharacterManager.h"
#include <boost\multi_index_container.hpp>
#include <boost\multi_index\member.hpp>
#include <boost\multi_index\ordered_index.hpp>

using namespace boost::multi_index;

namespace Baseball{
	class Team{
	public:
		struct pos_dajun_id{
			int pos;//10 is no used, 11 is used
			int dajun;//10 is no used, 11 is used
			int id;//-1 is no id
			
			pos_dajun_id(int pos_, int dajun_, int id_) 
						:pos(pos_), dajun(dajun_), id(id_){}
		};
		struct pos{};
		struct dajun{};
		struct id{};
		typedef multi_index_container<
			pos_dajun_id,
			indexed_by<
				ordered_non_unique<tag<pos>, member<pos_dajun_id, int, &pos_dajun_id::pos> >,
				ordered_non_unique<tag<dajun>, member<pos_dajun_id, int, &pos_dajun_id::dajun> >,
				ordered_non_unique<tag<id>, member<pos_dajun_id, int, &pos_dajun_id::id> >
			>
		>order_;
	private:
		order_ first_team;
		order_ second_team;
		order_ sub;
	public:
		Team();
		bool change_dajun(int f, int s, int team);
		bool change_pos(int f, int s, int team);
		bool change_id(int f, int f_team, int s, int s_team);//0 is first, 1 is second, 2 is sub;
		bool add_id(int id_);
		bool del_id(int id_);
		order_ get_order(int team);
	};
}