#include "Count.h"

bool Baseball::Count::add_ball(){
	ball++;
	if (ball >= 4){
		return false;
	}
	else{
		return true;
	}
}

bool Baseball::Count::add_strike(){
	strike++;
	if (strike >= 3){
		return false;
	}
	else{
		return true;
	}
}

bool Baseball::Count::add_out(){
	out++;
	if (out >= 3){
		return false;
	}
	else{
		return true;
	}
}

void Baseball::Count::reset_all(){
	reset_bs();
	out = 0;
}

void Baseball::Count::reset_bs(){
	ball = 0;
	strike = 0;
}

int Baseball::Count::get_ball(){
	return ball;
}

int Baseball::Count::get_out(){
	return out;
}

int Baseball::Count::get_strike(){
	return strike;
}