#include "CharacterManager.h"

shared_ptr<Baseball::Character> Baseball::CharacterManager::get(int id){
	if (chars.find(id) != chars.end()){
		return chars[id];
	}
	else{
		return nullptr;
	}
}

void Baseball::CharacterManager::destroy(int id){
	if (chars.find(id) != chars.end()){
		chars.erase(id);
	}
}

shared_ptr<Baseball::Character> Baseball::CharacterManager::create(){
	int id_ = 0;
	while (1){
		if (chars.find(id_) != chars.end()){
			break;
		}
		id_++;
	}
	auto chara = make_shared<Character>(id_);
	chars[id_] = chara;
	return chara;
}

void Baseball::CharacterManager::load_chars(string path){
	ifstream ifs(path.c_str());
	boost::archive::binary_iarchive arc(ifs);
}

void Baseball::CharacterManager::save_chars(string path){
	ofstream ofs(path.c_str());
	boost::archive::binary_oarchive arc(ofs);
}