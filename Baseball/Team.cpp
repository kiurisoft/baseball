#include "Team.h"

Baseball::Team::Team(){
	for (int i = 0; i != 9; i++){
		first_team.insert(pos_dajun_id(i, i, -1));
		second_team.insert(pos_dajun_id(i, i, -1));
		first_team.insert(pos_dajun_id(10, 10, -1));
		second_team.insert(pos_dajun_id(10, 10, -1));
	}
}

bool Baseball::Team::change_dajun(int f, int s, int team){
	if (0 <= f && f < 9 && 0 <= s && s < 9 && 0 <= team && team < 3){
		order_* ord = nullptr;
		switch (team){
		case 0:
			ord = &first_team;
			break;
		case 1:
			ord = &second_team;
			break;
		case 2:
			ord = &sub;
			break;
		}

		auto& m = (*ord).get<dajun>();
		auto f_itr = m.find(f);
		auto s_itr = m.find(s);
		auto f_ = *f_itr;
		auto s_ = *s_itr;
		std::swap(f_.dajun, s_.dajun);
		m.replace(f_itr, f_);
		m.replace(s_itr, s_);
		
		return true;
	}
	else{
		return false;
	}
}

bool Baseball::Team::change_pos(int f, int s, int team){
	if (0 <= f && f < 9 && 0 <= s && s < 9 && 0 <= team && team < 3){
		order_* ord = nullptr;
		switch (team){
		case 0:
			ord = &first_team;
			break;
		case 1:
			ord = &second_team;
			break;
		case 2:
			ord = &sub;
			break;
		}

		auto& m = (*ord).get<pos>();
		auto f_itr = m.find(f);
		auto s_itr = m.find(s);
		auto f_ = *f_itr;
		auto s_ = *s_itr;
		std::swap(f_.pos, s_.pos);
		m.replace(f_itr, f_);
		m.replace(s_itr, s_);

		return true;
	}
	else{
		return false;
	}
}

bool Baseball::Team::change_id(int f, int f_team, int s, int s_team){
	if ((!(f == -1 && s == -1)) && 0 <= f_team && f_team < 3 && 0 <= s_team && s_team < 3){
		order_* f_ord = nullptr;
		order_* s_ord = nullptr;
		switch (f_team){
		case 0:
			f_ord = &first_team;
			break;
		case 1:
			f_ord = &second_team;
			break;
		case 2:
			f_ord = &sub;
			break;
		}
		switch (s_team){
		case 0:
			s_ord = &first_team;
			break;
		case 1:
			s_ord = &second_team;
			break;
		case 2:
			s_ord = &sub;
			break;
		}

		auto& f_m = (*f_ord).get<id>();
		auto& s_m = (*s_ord).get<id>();
		auto f_itr = f_m.find(f);
		auto s_itr = s_m.find(s);
		auto f_ = *f_itr;
		auto s_ = *s_itr;
		std::swap(f_.id, s_.id);
		
		f_m.replace(f_itr, f_);
		s_m.replace(s_itr, s_);

		return true;
	}
	else{
		return false;
	}
}

bool Baseball::Team::add_id(int id_){
	if (id_ != -1){
		order_* ords[3] = {&first_team, &second_team, &sub};
		for (int i = 0; i != 3; i++){
			if (i != 2){
				auto& m = ords[i]->get<id>();
				auto itr = m.find(-1);
				if (itr != m.end()){
					auto t = *itr;
					t.id = id_;
					m.replace(itr, t);
					break;
				}
			}
			else{
				ords[i]->insert(pos_dajun_id(10, 10, id_));
			}
		}
		return true;
	}
	else {
		return false;
	}
}

bool Baseball::Team::del_id(int id_){
	if (id_ != -1){
		order_* ords[3] = {&first_team, &second_team, &sub};
		for (int i = 0; i != 3; i++){
			auto& m = ords[i]->get<id>();
			auto itr = m.find(id_);
			if (itr != m.end()){
				auto temp = *itr;
				temp.id = -1;
				m.replace(itr, temp);
				return true;
			}
		}
		return false;
	}
	else{
		return false;
	}
}

Baseball::Team::order_ Baseball::Team::get_order(int team){
	switch (team){
	case 0:
		return first_team;
	case 1:
		return second_team;
	case 2:
		return sub;
	default://error
		return sub;
	}
}