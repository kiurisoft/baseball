#include "Match.h"

Baseball::Match::Match(Baseball::Team::order_& f, Baseball::Team::order_& l) 
: finished(false){
	order[0] = f;
	order[1] = l;
}

bool Baseball::Match::is_finished(){
	return finished;
}

bool Baseball::Match::tourui(std::string& player_select){
	if (player_select == "tourui"){
		return true;
	}
	else{
		if (count.get_ball() == 3){
			if (count.get_strike() == 2){
				return true;
			}
			else{
				//random
			}
		}
		else if (count.get_ball() == 2){
			if (count.get_strike() != 0){
				//random
			}
		}
		return false;
	}
}

bool Baseball::Match::kensei(std::string& player_select){
	if (player_select == "kensei"){
		return true;
	}
	else{
		return false;
	}
}

void Baseball::Match::step(std::string& player_select){
	if (is_finished() != true){
		if (tourui(player_select) == true){}
		if (kensei(player_select) == true){}
		toukyu(player_select);
		dakyu(player_select);
		shubi(player_select);
	}
}