#pragma once

#include <boost\multi_index_container.hpp>
#include <boost\multi_index\member.hpp>
#include <boost\multi_index\ordered_index.hpp>

using namespace boost::multi_index;

namespace Baseball{
	class UZR{
	public:
		struct data{
			int field_number;
			int dakyu_sokudo;
			int dakyu_shurui;
			int pos;
			int id;

			data(int fn, int dso, int dsh, int po, int id_) 
				:field_number(fn), dakyu_sokudo(dso), dakyu_shurui(dsh),
				 pos(po), id(id_){}
		};

		typedef multi_index_container<
			data,
			indexed_by<
				ordered_non_unique<member<data, int, &data::field_number> >,
				ordered_non_unique<member<data, int, &data::dakyu_sokudo> >,
				ordered_non_unique<member<data, int, &data::dakyu_shurui> >,
				ordered_non_unique<member<data, int, &data::pos> >,
				ordered_non_unique<member<data, int, &data::id> >
			>
		>UZR_data_;
	private:
		UZR_data_ UZR_data;
	public:
		void add(data& d);
		void erase(int id);
		double calc_uzr(int pos_, int id_);
	};
}